defmodule Cookbook.Repo.Migrations.CreatePlanningMembersTable do
  use Ecto.Migration

  def change do
    create table(:planning_members) do
      add :is_admin, :boolean, default: false
      add :can_edit, :boolean, default: false
      add :notifications_enabled, :boolean, default: true

      add :planning_id, references(:plannings, on_delete: :delete_all), null: false
      add :member_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
