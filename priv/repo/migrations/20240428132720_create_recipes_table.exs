defmodule Cookbook.Repo.Migrations.CreateRecipesTable do
  use Ecto.Migration

  def change do
    create table(:recipes) do
      add :title, :string, null: false
      add :description, :text, null: false
      add :preparation_time, :integer, null: false
      add :cook_time, :integer, null: false
      add :servings, :integer, null: false
      add :directions, {:array, :text}, null: false
      add :topics, {:array, :string}, null: false

      add :author_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
