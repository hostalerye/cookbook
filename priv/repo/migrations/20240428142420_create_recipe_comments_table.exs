defmodule Cookbook.Repo.Migrations.CreateRecipeCommentsTable do
  use Ecto.Migration

  def change do
    create table(:recipe_comments) do
      add :comment, :text, null: false

      add :recipe_id, references(:recipes, on_delete: :delete_all), null: false
      add :author_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
