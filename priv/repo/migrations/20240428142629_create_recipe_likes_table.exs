defmodule Cookbook.Repo.Migrations.CreateRecipeLikesTable do
  use Ecto.Migration

  def change do
    create table(:recipe_likes) do
      add :notifications_enabled, :boolean, null: false, default: true

      add :recipe_id, references(:recipes, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
