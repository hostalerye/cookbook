defmodule Cookbook.Repo.Migrations.CreatePlanningMealCommentsTable do
  use Ecto.Migration

  def change do
    create table(:planning_meal_comments) do
      add :comment, :text, null: false

      add :planning_meal_id, references(:planning_meals, on_delete: :delete_all), null: false
      add :author_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
