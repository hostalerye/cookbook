defmodule Cookbook.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :measurement, :string, null: false

      timestamps()
    end
  end
end
