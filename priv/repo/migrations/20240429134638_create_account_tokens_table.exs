defmodule Cookbook.Repo.Migrations.CreateAccountTokensTable do
  use Ecto.Migration

  def change do
    create table(:account_tokens) do
      add :token, :binary, null: false
      add :context, :string, null: false
      add :sent_to, :string

      add :account_id, references(:accounts, on_delete: :delete_all), null: false

      timestamps(updated_at: false)
    end

    create index(:account_tokens, [:account_id])
    create unique_index(:account_tokens, [:context, :token])
  end
end
