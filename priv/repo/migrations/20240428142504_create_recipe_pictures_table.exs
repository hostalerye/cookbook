defmodule Cookbook.Repo.Migrations.CreateRecipePicturesTable do
  use Ecto.Migration

  def change do
    create table(:recipe_pictures) do
      add :path, :string, null: false

      add :recipe_id, references(:recipes, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
