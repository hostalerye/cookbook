defmodule Cookbook.Repo.Migrations.CreateRecipeIngredientsTable do
  use Ecto.Migration

  def change do
    create table(:recipe_ingredients) do
      add :quantity, :decimal, null: false
      add :unit, :string, null: false

      add :ingredient_id, references(:ingredients, on_delete: :delete_all), null: false
      add :recipe_id, references(:recipes, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
