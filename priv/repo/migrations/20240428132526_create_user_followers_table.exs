defmodule Cookbook.Repo.Migrations.CreateUserFollowersTable do
  use Ecto.Migration

  def change do
    create table(:user_followers) do
      add :notifications_enabled, :boolean, default: true

      add :followee_id, references(:users, on_delete: :delete_all), null: false
      add :follower_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
