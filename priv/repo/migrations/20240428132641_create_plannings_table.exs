defmodule Cookbook.Repo.Migrations.CreatePlanningsTable do
  use Ecto.Migration

  def change do
    create table(:plannings) do
      add :title, :string, null: false

      timestamps()
    end
  end
end
