defmodule Cookbook.Repo.Migrations.CreatePlanningMealsTable do
  use Ecto.Migration

  def change do
    create table(:planning_meals) do
      add :meal, :string, null: false
      add :date, :date, null: false

      add :planning_id, references(:plannings, on_delete: :delete_all), null: false
      add :recipe_id, references(:recipes, on_delete: :delete_all), null: false
      add :added_by_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end
  end
end
