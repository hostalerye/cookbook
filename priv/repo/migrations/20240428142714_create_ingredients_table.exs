defmodule Cookbook.Repo.Migrations.CreateIngredientsTable do
  use Ecto.Migration

  def change do
    create table(:ingredients) do
      add :name, :string, null: false

      timestamps()
    end
  end
end
