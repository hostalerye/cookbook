defmodule Cookbook.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Cookbook.Accounts` context.
  """

  def unique_account_email, do: "account#{System.unique_integer()}@example.com"
  def valid_account_password, do: "Hello42world!"

  def valid_account_attributes(attrs \\ %{}) do
    Enum.into(attrs, %{
      email: unique_account_email(),
      password: valid_account_password(),
      first_name: "John",
      last_name: "Doe"
    })
  end

  def account_fixture(attrs \\ %{}) do
    {:ok, %{account: account}} =
      attrs
      |> valid_account_attributes()
      |> Cookbook.Accounts.register_user_account()

    account
  end

  def extract_account_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end
end
