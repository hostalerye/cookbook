defmodule Cookbook.Schemas.User do
  @moduledoc false
  use Ecto.Schema

  @measurements [
    :metric,
    :imperial
  ]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :measurement, Ecto.Enum, values: @measurements

    has_many :accounts, Cookbook.Schemas.Account

    timestamps()
  end
end
