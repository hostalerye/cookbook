defmodule Cookbook.Schemas.RecipeIngredient do
  @moduledoc false
  use Ecto.Schema

  @units [
    :liter,
    :gram,
    :gallon,
    :pound,
    :ounce,
    :pint,
    :quart,
    :teaspoon,
    :tablespoon,
    :unit
  ]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "recipe_ingredients" do
    field :unit, Ecto.Enum, values: @units
    field :quantity, :decimal

    belongs_to :ingredient, Cookbook.Schemas.Ingredient
    belongs_to :recipe, Cookbook.Schemas.Recipe

    timestamps()
  end
end
