defmodule Cookbook.Schemas.Planning do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "plannings" do
    field :title, :string

    has_many :members, Cookbook.Schemas.PlanningMember
    has_many :meals, Cookbook.Schemas.PlanningMeal

    timestamps()
  end
end
