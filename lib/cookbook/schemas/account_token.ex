defmodule Cookbook.Schemas.AccountToken do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "account_tokens" do
    field :token, :binary
    field :context, :string
    field :sent_to, :string

    belongs_to :account, Cookbook.Schemas.Account

    timestamps(updated_at: false)
  end
end
