defmodule Cookbook.Schemas.Recipe do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "recipes" do
    field :title, :string
    field :description, :string
    field :preparation_time, :integer
    field :cook_time, :integer
    field :servings, :integer
    field :directions, {:array, :string}
    field :topics, {:array, :string}

    belongs_to :author, Cookbook.Schemas.User

    has_many :ingredients, Cookbook.Schemas.RecipeIngredient
    has_many :comments, Cookbook.Schemas.RecipeComment
    has_many :likes, Cookbook.Schemas.RecipeLike
    has_many :pictures, Cookbook.Schemas.RecipePicture

    timestamps()
  end
end
