defmodule Cookbook.Schemas.PlanningMember do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "planning_members" do
    field :is_admin, :boolean, default: false
    field :can_edit, :boolean, default: false
    field :notifications_enabled, :boolean, default: true

    belongs_to :planning, Cookbook.Schemas.Planning
    belongs_to :member, Cookbook.Schemas.User

    timestamps()
  end
end
