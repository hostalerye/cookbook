defmodule Cookbook.Schemas.PlanningMeal do
  @moduledoc false
  use Ecto.Schema

  @meals [
    :breakfast,
    :brunch,
    :elevenses,
    :lunch,
    :tea,
    :supper,
    :dinner
  ]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "planning_meals" do
    field :meal, Ecto.Enum, values: @meals
    field :date, :date

    belongs_to :planning, Cookbook.Schemas.Planning
    belongs_to :recipe, Cookbook.Schemas.Recipe
    belongs_to :added_by, Cookbook.Schemas.User

    has_many :comments, Cookbook.Schemas.PlanningMealComment

    timestamps()
  end
end
