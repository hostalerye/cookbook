defmodule Cookbook.Schemas.Ingredient do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "ingredients" do
    field :name, :string

    timestamps()
  end
end
