defmodule Cookbook.Schemas.RecipeLike do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "recipe_likes" do
    field :notifications_enabled, :boolean, default: true

    belongs_to :recipe, Cookbook.Schemas.Recipe
    belongs_to :user, Cookbook.Schemas.User

    timestamps()
  end
end
