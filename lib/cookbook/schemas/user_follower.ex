defmodule Cookbook.Schemas.UserFollower do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "user_followers" do
    field :notifications_enabled, :boolean, default: true

    belongs_to :followee, Cookbook.Schemas.User
    belongs_to :follower, Cookbook.Schemas.User

    timestamps()
  end
end
