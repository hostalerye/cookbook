defmodule Cookbook.Schemas.RecipeComment do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "recipe_comments" do
    field :comment, :string

    belongs_to :recipe, Cookbook.Schemas.Recipe
    belongs_to :author, Cookbook.Schemas.User

    timestamps()
  end
end
