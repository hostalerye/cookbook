defmodule Cookbook.Schemas.PlanningMealComment do
  @moduledoc false
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "planning_meal_comments" do
    field :comment, :string

    belongs_to :planning_meal, Cookbook.Schemas.PlanningMeal
    belongs_to :author, Cookbook.Schemas.User

    timestamps()
  end
end
