defmodule Cookbook.Accounts.User do
  @moduledoc false
  import Ecto.Changeset

  def registration_changeset(account, attrs) do
    account
    |> cast(attrs, [:first_name, :last_name])
    |> validate_required([:first_name, :last_name])
    |> put_change(:measurement, :imperial)
  end
end
